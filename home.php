<?php
    $title ="Dashboard";
include "includes/header.php";
?>
    <div class="wrapper">
    <?php
$dash = "nav-item active";  
include "includes/sidebar.php";
?>
    
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Current Statistics</h4>
                                    <p class="card-category">Current statistic of COVID 19</p>
                                </div>
                                <div class="card-body ">
                                
                                <img src="vendors/kendo/Material/loading-image.gif" id ="loaderImage1" class="ct-chart ct-perfect-fourth loader1" >
                                    <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                                    <div class="legend" style="margin-top: 9%;">
                                        <i class="fa fa-circle text-info"></i> Active
                                        <i class="fa fa-circle text-danger"></i> Death
                                        <i class="fa fa-circle text-warning"></i> Recovered
                                    </div>
                                    <hr>
                                    <div class="stats">
                                    <i class="fa fa-line-chart"></i> <span id="worldStat">Loading...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">COVID 19 Nepal Statistic</h4>
                                    <p class="card-category">Monthly evaluation</p>
                                </div>
                                <div class="card-body ">
                                <img src="vendors/kendo/Material/loading-image.gif" id ="loaderImage2" class="ct-chart loader2" >
                                    <div id="chartHours" class="ct-chart"></div>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Confirmed
                                        <i class="fa fa-circle text-danger"></i> Death
                                        <i class="fa fa-circle text-warning"></i> Recovered
                                    </div>
                                    <hr>
                                    <div class="stats">
                                  
                                    <i class="fa fa-line-chart"></i> <span id="nepalStat">Loading...</span>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Countries</h4>
                                    <p class="card-category">Country Wise Stats</p>
                                </div>
                                <div class="card-body ">
                                    <div id="grid" class="ct-chart"></div>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        
                                    </div>
                                    <hr>
                                    <div class="stats">
                                      </div>
                                </div>

      
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card  card-tasks">
                                <div class="card-header ">
                                    <h4 class="card-title">Top Stats</h4>
                                    <p class="card-category">Top Stats</p>
                                </div>
                                <div class="card-body ">
                                    <div class="table-full-width">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                <i class="fa fa-line-chart"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Nepal's Population</td>
                                                   <td><span id="populatinCount">0 </span></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                            <i class="fa fa-users"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total Suspect Reported</td>
                                                   <td><span id="suspectCount">0 </span></td>
                                                </tr>
                                                <tr>
                                                <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                            <i class="fa fa-phone"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total Emergency Contact Added</td>
                                                   <td><span id="emergencyCount">0 </span> </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                           
                        <i class="fa fa-hospital-o"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total Hospital Enlisted</td>
                                                   <td><span id="hospitalCount">0 </span> </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                            <i class="fa fa-rss-square"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total News Added</td>
                                                   <td><span id="newsCount">0 </span></td>
                                                </tr>
                                                <tr>
                                         
                                                </tr>
                                                <tr>
                                             
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <hr>
                                    <div class="stats">
                                        <i class="now-ui-icons loader_refresh spin"></i> 
                                    </div>
                                </div>
                            </div>

                            <div class="card ">
                            <div class="card-header ">
                                    <h4 class="card-title">Supported by</h4>
                                </div>
                                <div class="card-body ">
                                <div class="row">
                

<div class="col-md-6"> <p class="card-category">
<img src="assets/img/nyayahealth.png" class="supporter" />  Nyaya Health Nepal</p>
</div>


<div class="col-md-6"> <p class="card-category">
<img src="assets/img/possible.jpg" class="supporter" /> Possible Health</p>
</div>



</div>

</div>
</div>
                        </div>
                    </div>
                </div>
            </div>
           <?php


$js = '<script src="assets/js/plugins/chartist.min.js"></script><script src="assets/customJs/custom_home.js"></script>';
include "includes/footer.php";
?>

<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        // demo.initDashboardPageCharts();

        // demo.showNotification();

    });
</script>