<?php

session_start();
if($_GET["logout"] == "yes")
{
	session_destroy();
}

?>
 <!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/coronavirus.png">
    <link rel="icon" type="image/png" href="assets\img\coronavirus.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>COVID 19 Web Admin | <?php echo $title ; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <link rel="stylesheet" href="vendors/kendo/kendo.common-material.min.css" />
    <link rel="stylesheet" href="vendors/kendo/kendo.material.min.css" />
    <link rel="stylesheet" href="assets/css/custom_css.css" />

    <script src="vendors/jquery/jquery.min.js"></script>
    
    
    <script src="vendors/kendo/kendo.all.min.js"></script>

</head>

<body>
    <div class="wrapper">
	
        <div class="main-panel" style="float: none;width: fit-content;">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#pablo"> <?php echo $title ; ?> </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown">
                                    <span class="d-lg-none" style="display: block !important;font-size: xx-large;">COVID 19 Impact</span>
                                </a>
                            </li>
                           
                        </ul>
                        <ul class="navbar-nav ml-auto">
                         
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <span class="no-icon"><a href="login.php">Admin login</a></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
    
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">Current Statistics</h4>
                                    <p class="card-category">Current statistic of COVID 19</p>
                                </div>
                                <div class="card-body " style="height: 427px;">
                                
                                <img src="vendors/kendo/Material/loading-image.gif" id ="loaderImage1" class="ct-chart ct-perfect-fourth loader1" >
                                    <div id="chartPreferences" class="ct-chart ct-perfect-fourth"></div>
                                    <div class="legend" >
                                        <i class="fa fa-circle text-info"></i> Active
                                        <i class="fa fa-circle text-danger"></i> Death
                                        <i class="fa fa-circle text-warning"></i> Recovered
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-line-chart"></i> <span id="worldStat">Loading...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card ">
                                <div class="card-header ">
                                    <h4 class="card-title">COVID 19 Nepal Statistic</h4>
                                    <p class="card-category">Monthly evaluation</p>
                                </div>
                                <div class="card-body ">
                                <img src="vendors/kendo/Material/loading-image.gif" id ="loaderImage2" class="ct-chart loader2" >
                                    <div id="chartHours" class="ct-chart"></div>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        <i class="fa fa-circle text-info"></i> Confirmed
                                        <i class="fa fa-circle text-danger"></i> Death
                                        <i class="fa fa-circle text-warning"></i> Recovered
                                    </div>
                                    <hr>
                                    <div class="stats">
                                        <i class="fa fa-line-chart"></i> <span id="nepalStat">Loading...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card" style="height: 667px;">
                                <div class="card-header ">
                                    <h4 class="card-title">Countries</h4>
                                    <p class="card-category">Country Wise Stats</p>
                                </div>
                                <div class="card-body " >
                                    <div id="grid" class="ct-chart"></div>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        
                                    </div>
                                    <hr>
                                    <div class="stats">
                                    <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>

      
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card  card-tasks">
                                <div class="card-header ">
                                    <h4 class="card-title">Top Stats</h4>
                                    <p class="card-category">Top Stats</p>
                                </div>
                                <div class="card-body ">
                                    <div class="table-full-width">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                <i class="fa fa-line-chart"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Nepal's Population</td>
                                                   <td><span id="populatinCount">0 </span></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                            <i class="fa fa-users"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total Suspect Reported</td>
                                                   <td><span id="suspectCount">0 </span></td>
                                                </tr>
                                                <tr>
                                                <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                            <i class="fa fa-phone"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total Emergency Contact Added</td>
                                                   <td><span id="emergencyCount">0 </span> </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                           
                        <i class="fa fa-hospital-o"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total Hospital Enlisted</td>
                                                   <td><span id="hospitalCount">0 </span> </td>
                                                </tr>
                                                <tr>
                                                <td>
                                                        <div class="form-check">
                                                            <label class="form-check-label">
                                                            <i class="fa fa-rss-square"></i>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>Total News Added</td>
                                                   <td><span id="newsCount">0 </span></td>
                                                </tr>
                                                <tr>
                                         
                                                </tr>
                                                <tr>
                                             
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <hr>
                                    <div class="stats">
                                        <i class="now-ui-icons loader_refresh spin"></i> 
                                    </div>
                                </div>
                            </div>

                            <div class="card ">
                            <div class="card-header ">
                                    <h4 class="card-title">Supported by</h4>
                                </div>
                                <div class="card-body ">
                                <div class="row">
                

<div class="col-md-6"> <p class="card-category">
<img src="assets/img/nyayahealth.png" class="supporter" />  Nyaya Health Nepal</p>
</div>


<div class="col-md-6"> <p class="card-category">
<img src="assets/img/possible.jpg" class="supporter" /> Possible Health</p>
</div>



</div>

</div>
</div>
                        </div>
                    </div>
                </div>
            </div>
           <?php


$js = '<script src="assets/js/plugins/chartist.min.js"></script><script src="assets/customJs/custom_home.js"></script>';
include "includes/footer.php";
?>

<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        // demo.initDashboardPageCharts();

        // demo.showNotification();

    });
</script>