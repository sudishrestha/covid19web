<?php
    $title ="Suspects";
include "includes/header.php";

?>
    <div class="wrapper">
    <?php
$suspects = "nav-item active"; 
include "includes/sidebar.php";

?>
    
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
<!-- from here -->
<div id="example">
    <div id="grid"></div>
</div>

<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>

        <!-- <upto here -->
</div>
</div>

<script type="text/x-kendo-template" id="template">
		<a id="refresh" class="btn btn-warning btn-sm k-grid-refresh" data-toggle="tooltip" title="Refresh"><span class="fa fa-refresh "></span>
			Refresh</a>
		<a id="toggle" class="btn btn-warning btn-sm k-grid-refresh" data-toggle="tooltip" title="Toggle status"><span class="fa fa-toggle-on "></span>
			Toggle Status</a>
	</script>
           <?php

$js = '<script src="assets/customJs/customContacts.js"></script>';
include "includes/footer.php";
?>