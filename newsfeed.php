<?php
    $title ="News Feeds";
include "includes/header.php";

?>
    <div class="wrapper">
    <?php
    
$feeds = "nav-item active";  
include "includes/sidebar.php";

?>
    
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
<!-- from here -->
<div id="example">
    <div id="grid"></div>
</div>

<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>

<!-- modal  -->
<div id="newsModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-lg">
      <!-- <form action="apidata/driver_page/create_driver.php" id="form" class="form-horizontal" method="post" enctype="multipart/form-data"> -->
      <input type="hidden" name="driver_id" id="driver_id">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button"  id="modClose"  class="close" data-toggle="modal-close">&times;</button>
            <h4 class="modal-title">
            </h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="iconurl">Icon Image </label>
                     <input type="text" class="form-control" name="iconurl" id="iconurl" required>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="newsImage">News Image</label>
                     <input type="text" class="form-control" name="newsImage" id="newsImage" required>
                  </div>
               </div>
              
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="newsLink">News Link</label>
                     <input type="text" class="form-control" name="newsLink" id="newsLink" onInput="getMeta();" required>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="name">Source</label>
                     <input type="text" class="form-control" name="source" id="source" required>
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="form-group">
                     <label for="p_address">Title</label>
                     <input type="text" class="form-control" name="title" id="title" required>
                  </div>
               </div>
              
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group">
                     <label for="contact_no">Description</label>
                     <input type="text" class="form-control" name="desc" id="desc" required>
                  </div>
               </div>
             
            </div>
        
             
             
            <div class="modal-footer ">
               <button type="submit" id="btn-save" class="btn btn-success btn-sm pull-right">
               <i class="fa fa-save"></i> Save
               </button>
               <button type="button" id="modClose" class="btn btn-danger btn-sm pull-right" data-toggle="modal-close">
               <i class="fa fa-close"></i> Close
               </button>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal end -->
        <!-- <upto here -->
</div>
</div>

<script type="text/x-kendo-template" id="template">
		<a id="add" class="btn btn-success btn-sm " data-toggle="tooltip" title="Add"><span class="fa fa-plus"></span>Add</a>
		<a id="delete" class="btn btn-danger btn-sm k-grid-delete" data-toggle="tooltip" title="Delete"><span class="fa fa-times"></span>
			Delete</a>
		<a id="refresh" class="btn btn-warning btn-sm k-grid-refresh" data-toggle="tooltip" title="Refresh"><span class="fa fa-refresh "></span>
			Refresh</a>
	</script>
           <?php

$js = '<script src="assets/customJs/custom_feed.js"></script>';
include "includes/footer.php";
?>