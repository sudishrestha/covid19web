<?php
    $title ="Covid Stats";
include "includes/header.php";

?>
    <div class="wrapper">
    <?php
$stats = "nav-item active"; 
include "includes/sidebar.php";

?>
    
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
<!-- from here -->
<div id="example">
    <div id="grid"></div>
</div>

<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 32px;
        height: 32px;
        border-radius: 50%;
        background-size: 32px 35px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>

<!-- modal  -->
<div id="statModel" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button"  id="modClose"  class="close" data-toggle="modal-close">&times;</button>
            <h4 class="modal-title">
            </h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-8">
                  <div class="form-group">
                   
                     <label for="iconurl">Region</label>
                     <?php
                           include 'api/countryName.php';
                     ?>
                     <select  class="form-control" name="region" id="region" required>
                        <?php
                              for($i=0;$i < sizeof($countries);$i++)
                              {
                                ?> <option  value="<?php echo $countries[$i];?>"><?php echo $countries[$i];?> </option><?php
                              }

                        ?>
                     </select>
                     <!-- <input type="text" class="form-control" name="region" id="region" required> -->
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="newsImage">Updated Date</label>
                     <input type="text" class="form-control" name="updated_time" id="updated_time" disabled value="<?php echo   date("Y-m-d h:m:s");?>">
                  </div>
               </div>
              
             
            </div>

            <div class="row">
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="name">Confirmed</label>
                     <input type="text" class="form-control" name="confirm" id="confirm" required>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="p_address">Death</label>
                     <input type="text" class="form-control" name="death" id="death" required>
                  </div>
               </div>
              
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="p_address">Recovered</label>
                     <input type="text" class="form-control" name="recover" id="recover" required>
                  </div>
               </div>
            </div>
           
             
             
            <div class="modal-footer ">
               <button type="submit" id="btn-save" class="btn btn-success btn-sm pull-right">
               <i class="fa fa-save"></i> Save
               </button>
            
            </div>
         </div>
      </div>
   </div>
</div>
<!-- modal end -->
        <!-- <upto here -->
</div>
</div>


		<!-- <a id="import" class="btn btn-success btn-sm " data-toggle="tooltip" title="Import"><span class="fa fa-upload"></span>Import</a>
		<a id="add" class="btn btn-success btn-sm " data-toggle="tooltip" title="Add"><span class="fa fa-plus"></span>Add</a>
		<a id="delete" class="btn btn-danger btn-sm k-grid-delete" data-toggle="tooltip" title="Delete"><span class="fa fa-times"></span>
			Delete</a> -->
<script type="text/x-kendo-template" id="template">
		<a id="refresh" class="btn btn-warning btn-sm k-grid-refresh" data-toggle="tooltip" title="Refresh"><span class="fa fa-refresh "></span>
			Refresh</a>
	</script>
           <?php

$js = '<script src="assets/customJs/customStat.js"></script>';
include "includes/footer.php";
?>