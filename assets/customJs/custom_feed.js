var record_num = 0;
$(document).ready(function () {
	var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
		dataSource = new kendo.data.DataSource({
			transport: {
				read: "api/getFeed.php",
			},
			batch: true,
			pageSize: 10,
			editable: "true",
			serverPaging: true,
			serverFiltering: true,

			serverSorting: true,
			schema: {

				
				data: "resource",
				total: function (data) {
					return data['meta']['count'];
				}
			}
		});
	$("#grid").kendoGrid({
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Starts with",
					contains: "Contains",
					isnull: "Null",
					doesnotcontain: "Doesnot Contain"
				},

				number: {
					startswith: "Starts with",
					contains: "Contains",
					eq: "Is equal to",
					isnull: "Null",
				},
				date: {
					gte: "From Date",
					lte: "To Date",
					eq: "Equal To"
				},

			}
		},
		sortable: true,
		dataSource: dataSource,
		pageable: {
			refresh: true,
			pageSizes: [10, 25, 50, 100],
			buttonCount: 10
		},
		height: 800,
		selectable: true,
		toolbar: kendo.template($("#template").html()),
		columns: [{
			title: "S.N",
			template: "#= ++record #",
			width: "40px"
		},
		{
			field: "source",
			title: "Source",
			width: "50px"
		},
			{
				field: "feedTitle",
				title: "Title",
				width: "150px"
			}, {
				field: "feedDescription",
				title: "Description",
				width: "170px"
			},
			
		],
		editable: {
			mode: "popup",
			confirmation: false
		},

		dataBinding: function () {
			record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
		}
	});

	$("#refresh").click(function (e) {
		$("#grid").data("kendoGrid").dataSource.filter({});
		e.preventDefault();
		$("#grid").data("kendoGrid").dataSource.read();
	});

	
	$("#add").click(function (e) {	
		$('#newsModal').modal('show');
	});
	$("#delete").click(function (e) {	
		var grid = $("#grid").data("kendoGrid");
var selected = [];
grid.select().each(function(){
    selected.push(grid.dataItem(this));
})
		// alert("to be deleted"+selected[0]["feedId"]);
		var feed_ids =selected[0]["feedId"];
		if (selected.length == 0) {
			$.notify({
				icon: "nc-icon nc-app",
				message: "Select Item to be deleted!!!"
		
			}, {
				type: type[2],
				timer: 8000,
				placement: {
					from: 'top',
					align: 'right'
				}
			});
			return false;
		}

		$.ajax
		({
			type: 'post',
			url: 'api/feedControl.php',
			data: {
				feed_id: feed_ids,
				method: 'delete'
			},
			success: function (response) {
			
				$('#newsModal').modal('hide');	
				$("#grid").data("kendoGrid").dataSource.filter({});
				e.preventDefault();
				$("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "Selected data is deleted!!!"
				
					}, {
						type: type[2],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
		return false;
	});




	$("#modClose").click(function (e) {
		$('#newsModal').modal('hide');
	});

	$("#btn-save").click(function (e) {

	var iconImage = $("#iconurl").val();
	var newsImage = $("#newsImage").val();
	var source = $("#source").val();
	var title = $("#title").val();
	var description = $("#desc").val();
	var link = $("#newsLink").val();
	$.ajax
		({
			type: 'post',
			url: 'api/feedControl.php',
			data: {
				iconImage: iconImage,
				newsImage: newsImage,
				source: source,
				title: title,
				description: description,
				link: link,
				method: 'post'
			},
			success: function (response) {
			
				$('#newsModal').modal('hide');	
				$("#grid").data("kendoGrid").dataSource.filter({});
				e.preventDefault();
				$("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "New Data is Added!!!"
				
					}, {
						type: type[1],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
	});

});


function getMeta()
{
	var sources = $("#newsLink").val();
	

	$.ajax
		({
			type: 'post',
			url: 'api/getMeta.php',
			data: {
				url: sources
			},
			success: function (response) {
				var obj = JSON.parse(response);
				debugger;

				try{
					$("#source").val(obj["metaTags"]["web_author"]["value"]);
				}
				catch(ex)
				{
			
				}
				try{
					$("#title").val(obj["title"]);
				}
				catch(ex)
				{
			
				}
				try{
					$("#desc").val(obj["metaTags"]["description"]["value"]);
				}
				catch(ex)
				{
			
				}
				putData("#iconurl","");
				putData("#newsImage","");
				// putData("#title",obj["title"]);
				// putData("#desc",obj["metaTags"]["description"]["value"]);


				// $("#iconurl").val();
				// $("#newsImage").val();
				// $("#source").val(obj["metaTags"]["web_author"]["value"]);
				// $("#title").val(obj["title"]);
				// $("#desc").val(obj["metaTags"]["description"]["value"]);
				
			}
		});

}

function putData(ids,data)
{
	try{
		$(ids).val(data);
	}
	catch(ex)
	{

	}
}