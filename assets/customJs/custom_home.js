


	$.ajax
    ({
        type: 'get',
        url: 'https://corona.lmao.ninja/v2/historical/Nepal',
        success: function (response) { 
            document.getElementById("loaderImage2").style.display = "none";   
            fillImg2(response);
            
        }
    });

    
	$.ajax
    ({
        type: 'get',
        url: 'https://corona.lmao.ninja/all',
        success: function (response) { 
            document.getElementById("loaderImage1").style.display = "none";   

            fillData(response);
            
        }
    });
	$.ajax
    ({
        type: 'post',
        url: 'api/dashData.php',
        success: function (response) { 
            document.getElementById("populatinCount").innerHTML =response["resource"][4]["population"];
            document.getElementById("suspectCount").innerHTML = response["resource"][0]["suspects"];
            document.getElementById("emergencyCount").innerHTML = response["resource"][1]["contact"];
            document.getElementById("hospitalCount").innerHTML = response["resource"][2]["hospital"];
            document.getElementById("newsCount").innerHTML = response["resource"][3]["feeds"];

            
        }
    });


    var record_num = 0;
        var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
            dataSource = new kendo.data.DataSource({
                transport: {
                    read: "api/getLiveStat.php",
                },
                batch: true,
                pageSize: 5,
                editable: "true",
                serverPaging: true,
    
                schema: {
    
                    
                    data: "resource",
                    total: function (data) {
                        return 5;
                    }
                }
            });
        $("#grid").kendoGrid({
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        contains: "Contains",
                        isnull: "Null",
                        doesnotcontain: "Doesnot Contain"
                    },
    
                    number: {
                        startswith: "Starts with",
                        contains: "Contains",
                        eq: "Is equal to",
                        isnull: "Null",
                    },
                    date: {
                        gte: "From Date",
                        lte: "To Date",
                        eq: "Equal To"
                    },
    
                }
            },
            sortable: true,
            dataSource: dataSource,
            pageable: false,
            height: 420,
            selectable: true,
            columns: [
            {
                field: "region",
                title: "Region",
                width: "120px"
            },
            {
                field: "confirmed",
                title: "Confirmed",
                width: "120px"
            },
                {
                    field: "death",
                    title: "Death",
                    width: "120px"
                }, {
                    field: "recovered",
                    title: "Recovered",
                    width: "120px"
                }
                
            ],
            editable: {
                mode: "popup",
                confirmation: false
            },
    
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            }
        });



        function fillImg2(response)
        {

            var s = response["timeline"]["cases"];
            var keys = [];
            var keyAll = [];
            for(var k in s) {keyAll.push(k)};

            for(var i =keyAll.length-7;i< keyAll.length;i++)
            {
                keys.push(keyAll[i]);
            }

            var dateLabel=keys;
            var confirmData=[];
            var deathData=[];
            var recoveredData=[];
            for(var i=0;i< keys.length;i++)
            {
                confirmData.push(response["timeline"]["cases"][keys[i]]);
                deathData.push(response["timeline"]["deaths"][keys[i]]);
                recoveredData.push(response["timeline"]["recovered"][keys[i]]);
            
            }
            
            
            var data ="Cases:"+confirmData[confirmData.length -1]+" | Death:"+deathData[deathData.length -1]+" | Recovered: "+recoveredData[recoveredData.length -1];
            document.getElementById("nepalStat").innerHTML =data;
                var dataSales = {
                    labels:dateLabel,
                    series: [
                        confirmData,
                        deathData,
                        recoveredData
                    ]
                };
                var optionsSales = {
                    lineSmooth: false,
                    low: 0,
                    high: 25,
                    showArea: true,
                    height: "245px",
                    axisX: {
                        showGrid: false,
                    },
                    lineSmooth: Chartist.Interpolation.simple({
                        divisor: 3
                    }),
                    showLine: false,
                    showPoint: false,
                    fullWidth: false
                };
            
                var responsiveSales = [
                    ['screen and (max-width: 640px)', {
                        axisX: {
                            labelInterpolationFnc: function(value) {
                                return value[0];
                            }
                        }
                    }]
                ];
            
                var chartHours = Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);
        }
function fillData( response)
{

    var optionsPreferences = {
        donut: true,
        donutWidth: 40,
        startAngle: 0,
        total: 100,
        showLabel: false,
        axisX: {
            showGrid: false
        }
    };

var death = parseInt(response["deaths"]);
var confirm= parseInt(response["cases"]);
var recovered= parseInt(response["recovered"]);


var data ="Cases:"+confirm+" | Death:"+death+" | Recovered: "+recovered;
document.getElementById("worldStat").innerHTML =data;
var resulted= parseInt(death) + parseInt(recovered);
var deathp= ((death/confirm)*100).toFixed(2);
var recoverp= ((recovered/confirm)*100).toFixed(2);
var remainingn= (((confirm-resulted)/confirm)*100).toFixed(2);
    Chartist.Pie('#chartPreferences', {
        labels: [remainingn, deathp, recoverp],
        series: [remainingn, deathp, recoverp]
    });

}
     