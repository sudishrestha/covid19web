var record_num = 0;
$(document).ready(function () {
	var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
		dataSource = new kendo.data.DataSource({
			transport: {
				read: "api/getHospitalData.php",
			},
			batch: true,
			pageSize: 10,
			editable: "true",
			serverPaging: true,
			serverFiltering: true,

			serverSorting: true,
			schema: {

				
				data: "resource",
				total: function (data) {
					return data['meta']['count'];
				}
			}
		});
	$("#grid").kendoGrid({
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Starts with",
					contains: "Contains",
					isnull: "Null",
					doesnotcontain: "Doesnot Contain"
				},

				number: {
					startswith: "Starts with",
					contains: "Contains",
					eq: "Is equal to",
					isnull: "Null",
				},
				date: {
					gte: "From Date",
					lte: "To Date",
					eq: "Equal To"
				},

			}
		},
		sortable: true,
		dataSource: dataSource,
		pageable: {
			refresh: true,
			pageSizes: [10, 25, 50, 100],
			buttonCount: 10
		},
		height: 800,
		selectable: true,
		toolbar: kendo.template($("#template").html()),
		columns: [{
			title: "S.N",
			template: "#= ++record #",
			width: "70px"
		},
		{
			field: "hospitalName",
			title: "Hospital Name",
			width: "170px"
		},
        {
            field: "dateUpdated",
            title: "Updated",
            width: "150px"
        },
			{
				field: "screened",
				title: "Screened",
				width: "150px"
			}, {
				field: "positiveCase",
				title: "Positive Case",
				width: "170px"
            },
            {
				field: "negativeCase",
				title: "Negative Case",
				width: "170px"
			},
            {
				field: "qurantined",
				title: "Qurantined",
				width: "150px"
			},
            {
				field: "isolated",
				title: "Isolated",
				width: "150px"
			},
            {
				field: "totalppe",
				title: "Total PPE",
				width: "150px"
			},
            {
				field: "usedppe",
				title: "Used PPE",
				width: "150px"
			},
			
		],
		editable: {
			mode: "popup",
			confirmation: false
		},

		dataBinding: function () {
			record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
		}
	});

	$("#refresh").click(function (e) {
		$("#grid").data("kendoGrid").dataSource.filter({});
		e.preventDefault();
		$("#grid").data("kendoGrid").dataSource.read();
	});

	
	$("#add").click(function (e) {	
		$('#newsModal').modal('show');
    });
    
	$("#delete").click(function (e) {	
                var grid = $("#grid").data("kendoGrid");
                var selected = [];
                grid.select().each(function(){
                selected.push(grid.dataItem(this));
                })
            // alert("to be deleted"+selected[0]["feedId"]);
            var hospitalId =selected[0]["hospitalId"];
            if (selected.length == 0) {
                $.notify({
                    icon: "nc-icon nc-app",
                    message: "Select Item to be deleted!!!"
            
                }, {
                    type: type[2],
                    timer: 8000,
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
                return false;
            }

            $.ajax
            ({
                type: 'post',
                url: 'api/hospitalControl.php',
                data: {
                    hospitalId: hospitalId,
                    method: 'delete'
                },
                success: function (response) {
                
                    $('#newsModal').modal('hide');	
                    $("#grid").data("kendoGrid").dataSource.filter({});
                    e.preventDefault();
                    $("#grid").data("kendoGrid").dataSource.read();
                        $.notify({
                            icon: "nc-icon nc-app",
                            message: "Selected data is deleted!!!"
                    
                        }, {
                            type: type[2],
                            timer: 8000,
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });
                
                    
                }
		});
		return false;
	});




	$("#modClose").click(function (e) {
		$('#newsModal').modal('hide');
	});

	$("#btn-save").click(function (e) {

	$.ajax
		({
			type: 'post',
			url: 'api/hospitalControl.php',
			data: {
        
                hospitalName:$("#hospitalName").val(),
               screened :$("#screen").val(),
               positiveCase :$("#positive").val(),
                negativeCase:$("#negative").val(),
               qurantined:$("#qurantine").val(),
               isolated :$("#isolate").val(),
                totalppe:$("#total").val(),
                usedppe:$("#used").val(),
				method: 'post'
			},
			success: function (response) {
			
				$('#newsModal').modal('hide');	
				$("#grid").data("kendoGrid").dataSource.filter({});
				e.preventDefault();
				$("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "New Data is Added!!!"
				
					}, {
						type: type[1],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
	});

});
