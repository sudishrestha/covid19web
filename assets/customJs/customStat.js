var record_num = 0;
$(document).ready(function () {
	var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
		dataSource = new kendo.data.DataSource({
			transport: {
				read: "api/getLiveStat.php",
			},
			batch: true,
			pageSize: 250,
			schema: {

				
				data: "resource",
				total: function (data) {
					return 230;
				}
			}
		});

		//https://corona.lmao.ninja/v2/historical/Nepal
	$("#grid").kendoGrid({
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Starts with",
					contains: "Contains",
					isnull: "Null",
					doesnotcontain: "Doesnot Contain"
				},

				number: {
					startswith: "Starts with",
					contains: "Contains",
					eq: "Is equal to",
					isnull: "Null",
				},
				date: {
					gte: "From Date",
					lte: "To Date",
					eq: "Equal To"
				},

			}
		},
		sortable: true,
		dataSource: dataSource,
		pageable: {
			refresh: true,
			pageSizes: [10, 25, 50, 100],
			buttonCount: 10
		},
		height: 800,
		selectable: true,
		toolbar: kendo.template($("#template").html()),
		columns: [{
			title: "S.N",
			template: "#= ++record #",
			width: "50px"
		},
		{
			field: "region",
			title: "Region",
			width: "150px"
		},
		{
			field: "confirmed",
			title: "Confirmed",
			width: "150px"
		},
			{
				field: "death",
				title: "Death",
				width: "150px"
			}, {
				field: "recovered",
				title: "Recovered",
				width: "150px"
			},{
				field: "dateUpdated",
				title: "Updated",
				width: "170px"
			}
			
		],
		editable: {
			mode: "popup",
			confirmation: false
		},

		dataBinding: function () {
			record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
		}
	});

	$("#refresh").click(function (e) {
		$("#grid").data("kendoGrid").dataSource.filter({});
		e.preventDefault();
		$("#grid").data("kendoGrid").dataSource.read();
	});

	
	$("#add").click(function (e) {	
		$('#statModel').modal('show');
	});


	
	$("#import").click(function (e) {	
		$.ajax
		({
			type: 'post',
			url: 'api/importLiveData.php',
			data: {
			},
			success: function (response) {
           
				$("#grid").data("kendoGrid").dataSource.filter({});
		e.preventDefault();
		$("#grid").data("kendoGrid").dataSource.read();
				
		alert("Importing Data Completed!!!");
			}
		});
	});


	$("#delete").click(function (e) {	
		var grid = $("#grid").data("kendoGrid");
		var selected = [];
		grid.select().each(function(){
			selected.push(grid.dataItem(this));
		})
		// alert("to be deleted"+selected[0]["feedId"]);
		var statId =selected[0]["id"];
		if (selected.length == 0) {
			$.notify({
				icon: "nc-icon nc-app",
				message: "Select Item to be deleted!!!"
		
			}, {
				type: type[2],
				timer: 8000,
				placement: {
					from: 'top',
					align: 'right'
				}
			});
			return false;
		}

		$.ajax
		({
			type: 'post',
			url: 'api/statControl.php',
			data: {
				statId: statId,
				method: 'delete'
			},
			success: function (response) {
				$("#grid").data("kendoGrid").dataSource.filter({});
				e.preventDefault();
				$("#grid").data("kendoGrid").dataSource.read();
				$("#grid").data("kendoGrid").dataSource.filter({});
				e.preventDefault();
				$("#grid").data("kendoGrid").dataSource.read();
            $('#statModel').modal('hide');	
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
            $("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "Selected Data is Deleted!!!"
				
					}, {
						type: type[2],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
		return false;
	});




	$("#modClose").click(function (e) {
		$('#statModel').modal('hide');
	});

	$("#btn-save").click(function (e) {

	$.ajax
		({
			type: 'post',
			url: 'api/statControl.php',
			data: {
				region:  $("#region").val(),
				confirm:  $("#confirm").val(),
				death:  $("#death").val(),
				recover:  $("#recover").val(),
				method: 'post'
			},
			success: function (response) {
			
                $('#statModel').modal('hide');	
                $("#grid").data("kendoGrid").dataSource.filter({});
                e.preventDefault();
                $("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "New Data Added!!!"
				
					}, {
						type: type[1],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
	});


		$("#btn-save").click(function (e) {

	$.ajax
		({
			type: 'post',
			url: 'api/statControl.php',
			data: {
				region:  $("#region").val(),
				confirm:  $("#confirm").val(),
				death:  $("#death").val(),
				recover:  $("#recover").val(),
				method: 'post'
			},
			success: function (response) {
			
                $('#statModel').modal('hide');	
                $("#grid").data("kendoGrid").dataSource.filter({});
                e.preventDefault();
                $("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "New Data Added!!!"
				
					}, {
						type: type[1],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
	});

});
