var record_num = 0;
$(document).ready(function () {
	var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
		dataSource = new kendo.data.DataSource({
			transport: {
				read: "api/getEmergency.php",
			},
			batch: true,
			pageSize: 10,
			editable: "true",
			serverPaging: true,
			serverFiltering: true,

			serverSorting: true,
			schema: {

				
				data: "resource",
				total: function (data) {
					return data['meta']['count'];
				}
			}
		});
	$("#grid").kendoGrid({
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Starts with",
					contains: "Contains",
					isnull: "Null",
					doesnotcontain: "Doesnot Contain"
				},

				number: {
					startswith: "Starts with",
					contains: "Contains",
					eq: "Is equal to",
					isnull: "Null",
				},
				date: {
					gte: "From Date",
					lte: "To Date",
					eq: "Equal To"
				},

			}
		},
		sortable: true,
		dataSource: dataSource,
		pageable: {
			refresh: true,
			pageSizes: [10, 25, 50, 100],
			buttonCount: 10
		},
		height: 800,
		selectable: true,
		toolbar: kendo.template($("#template").html()),
		columns: [{
			title: "S.N",
			template: "#= ++record #",
			width: "50px"
		},
		{
			field: "name",
			title: "Name",
			width: "150px"
		},
		{
			field: "address",
			title: "Address",
			width: "150px"
		},
			{
				field: "telephone1",
				title: "Tel1",
				width: "150px"
			}, {
				field: "telephone2",
				title: "Tel2",
				width: "170px"
			},{
				field: "telephone3",
				title: "Tel3",
				width: "170px"
			},{
				field: "encharge",
				title: "Encharge",
				width: "170px"
			},
			
		],
		editable: {
			mode: "popup",
			confirmation: false
		},

		dataBinding: function () {
			record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
		}
	});

	$("#refresh").click(function (e) {
		$("#grid").data("kendoGrid").dataSource.filter({});
		e.preventDefault();
		$("#grid").data("kendoGrid").dataSource.read();
	});

	
	$("#add").click(function (e) {	
		$('#emergencyModel').modal('show');
	});
	$("#delete").click(function (e) {	
		var grid = $("#grid").data("kendoGrid");
var selected = [];
grid.select().each(function(){
    selected.push(grid.dataItem(this));
})
		// alert("to be deleted"+selected[0]["feedId"]);
		var contactId =selected[0]["contactId"];
		if (selected.length == 0) {
			$.notify({
				icon: "nc-icon nc-app",
				message: "Select Item to be deleted!!!"
		
			}, {
				type: type[2],
				timer: 8000,
				placement: {
					from: 'top',
					align: 'right'
				}
			});
			return false;
		}

		$.ajax
		({
			type: 'post',
			url: 'api/emergencyControl.php',
			data: {
				contactId: contactId,
				method: 'delete'
			},
			success: function (response) {
            $('#emergencyModel').modal('hide');	
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
            $("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "Selected Data is Deleted!!!"
				
					}, {
						type: type[2],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
		return false;
	});




	$("#modClose").click(function (e) {
		$('#emergencyModel').modal('hide');
	});

	$("#btn-save").click(function (e) {

	$.ajax
		({
			type: 'post',
			url: 'api/emergencyControl.php',
			data: {
				name:  $("#name").val(),
				address:  $("#address").val(),
				encharge:  $("#encharge").val(),
				tel1:  $("#tel1").val(),
				tel2:  $("#tel2").val(),
				tel3:  $("#tel3").val(),
				method: 'post'
			},
			success: function (response) {
			
                $('#emergencyModel').modal('hide');	
                $("#grid").data("kendoGrid").dataSource.filter({});
                e.preventDefault();
                $("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "New Data Added!!!"
				
					}, {
						type: type[1],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
	});

});
