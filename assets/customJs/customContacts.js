var record_num = 0;
$(document).ready(function () {
	var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
		dataSource = new kendo.data.DataSource({
			transport: {
				read: "api/getContact.php",
			},
			batch: true,
			pageSize: 10,
			editable: "true",
			serverPaging: true,
			serverFiltering: true,

			serverSorting: true,
			schema: {

				
				data: "resource",
				total: function (data) {
					return data['meta']['count'];
				}
			}
		});
	$("#grid").kendoGrid({
		filterable: {
			extra: false,
			operators: {
				string: {
					startswith: "Starts with",
					contains: "Contains",
					isnull: "Null",
					doesnotcontain: "Doesnot Contain"
				},

				number: {
					startswith: "Starts with",
					contains: "Contains",
					eq: "Is equal to",
					isnull: "Null",
				},
				date: {
					gte: "From Date",
					lte: "To Date",
					eq: "Equal To"
				},

			}
		},
		sortable: true,
		dataSource: dataSource,
		pageable: {
			refresh: true,
			pageSizes: [10, 25, 50, 100],
			buttonCount: 10
		},
		height: 800,
		selectable: true,
		toolbar: kendo.template($("#template").html()),
		columns: [{
			title: "S.N",
			template: "#= ++record #",
			width: "50px"
		},
		{
			field: "name",
			title: "Name",
			width: "150px"
		},
			{
				field: "address",
				title: "Address",
				width: "150px"
			}, {
				field: "personToContact",
				title: "Contact Person",
				width: "170px"
			}, {
				field: "contactNumber",
				title: "Contact Number",
				width: "170px"
			}, {
				field: "description",
				title: "Description",
				width: "170px"
			}, {
				field: "status",
				title: "Status",
				width: "170px",
				template: "#if(status==1){# InComplete #}else{# Completed #}#",
				

			}
			
		],
		editable: {
			mode: "popup",
			confirmation: false
		},

		dataBinding: function () {
			record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
		}
	});

	$("#refresh").click(function (e) {
		$("#grid").data("kendoGrid").dataSource.filter({});
		e.preventDefault();
		$("#grid").data("kendoGrid").dataSource.read();
	});

	$("#toggle").click(function (e) {	

		var grid = $("#grid").data("kendoGrid");
		var selected = [];
		grid.select().each(function(){
			selected.push(grid.dataItem(this));
		})
		// alert("to be deleted"+selected[0]["feedId"]);
		try{
			var personId =selected[0]["personId"];
			var toggleStatus =selected[0]["status"];
		}
		catch (ex)
		{
			$.notify({
				icon: "nc-icon nc-app",
				message: "Select Item to be toggled!!!"
		
			}, {
				type: type[1],
				timer: 8000,
				placement: {
					from: 'top',
					align: 'right'
				}
			});
			return false;
		}
		if (selected.length == 0) {
			$.notify({
				icon: "nc-icon nc-app",
				message: "Select Item to be toggled!!!"
		
			}, {
				type: type[1],
				timer: 8000,
				placement: {
					from: 'top',
					align: 'right'
				}
			});
			return false;
		}

		$.ajax
		({
			type: 'post',
			url: 'api/suspectControl.php',
			data: {
				personId: personId,
				status:toggleStatus,
				method: 'update'
			},
			success: function (response) {
            $('#emergencyModel').modal('hide');	
            $("#grid").data("kendoGrid").dataSource.filter({});
            e.preventDefault();
            $("#grid").data("kendoGrid").dataSource.read();
					$.notify({
						icon: "nc-icon nc-app",
						message: "Selected Data is updated!!!"
				
					}, {
						type: type[2],
						timer: 8000,
						placement: {
							from: 'top',
							align: 'right'
						}
					});
			
				
			}
		});
		return false;
	});




});
