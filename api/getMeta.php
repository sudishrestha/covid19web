<?php
function getUrlData($url) {
    $result = false;

    $contents = getUrlContents($url);

    if (isset($contents) && is_string($contents)) {
        $title = null;
        $metaTags = null;

        preg_match('/<title>([^>]*)<\/title>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) > 0) {
            $title = strip_tags($match[1]);
        }

        preg_match_all('/<[\s]*meta[\s]*name="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) == 3) {
            $originals = $match[0];
            $names = $match[1];
            $values = $match[2];

            if (count($originals) == count($names) && count($names) == count($values)) {
                $metaTags = array();

                for ($i = 0, $limiti = count($names); $i < $limiti; $i++) {
                    $metaTags[$names[$i]] = array(
                        'html' => htmlentities($originals[$i]),
                        'value' => $values[$i]
                    );
                }
            }
        }

        $result = array(
            'title' => $title,
            'metaTags' => $metaTags
        );
    }
    return $result;
}


function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0) {
    $result = false;

    $contents = @file_get_contents($url);

    // Check if we need to go somewhere else

    if (isset($contents) && is_string($contents)) {
        preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1) {
            if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections) {
                return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
            }

            $result = false;
        } else {
            $result = $contents;
        }
    }

    return $contents;
}

$url = $_POST["url"];
$test = getUrlData($url);  //Replace  with your URL here
// echo '<pre>';
// print_r($test);
echo json_encode($test,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

// Assuming the above tags are at www.example.com
// $tags = get_meta_tags('https://www.onlinekhabar.com/2020/04/851321');


// print_r($tags);
// Notice how the keys are all lowercase now, and
// how . was replaced by _ in the key.
// echo $tags['author'];       // name
// echo $tags['keywords'];     // php documentation
// echo $tags['description'];  // a php manual
// echo $tags['geo_position']; // 49.33;-86.59
?>