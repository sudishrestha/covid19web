<?php
date_default_timezone_set("Asia/Kathmandu");
function parseFilters( $filters , $count = 0 ) {
    $where = "";
    $intcount = 0;
    $noend= false;
    $nobegin = false;
    if ( isset( $filters['filters'] ) ) {
        $itemcount = count( $filters['filters'] );
        if ( $itemcount == 0 ) {
            $noend= true;
            $nobegin = true;
        } elseif ( $itemcount == 1 ) {
            $noend= true;
            $nobegin = true;
        } elseif ( $itemcount > 1 ) {
            $noend= false;
            $nobegin = false;
        }
        foreach ( $filters['filters'] as $key => $filter ) {
            if ( isset($filter['field'])) {
                switch ( $filter['operator'] ) {
                    case 'startswith':
                        $compare = "%20LIKE%20";
                        $field = $filter['field'];
                        $value = "" . $filter['value'] . "%";
                        break;
                    case 'contains':
                        $compare = "%20LIKE%20";
                        $field = $filter['field'];
                        $value = "'%" . $filter['value'] . "%'";
                        break;
                    case 'doesnotcontain':
                        $compare = "%20NOT%20LIKE%20";
                        $field = $filter['field'];
                        $value = "'%" . $filter['value'] . "%'";
                        break;
                    case 'endswith':
                        $compare = "%20LIKE%20";
                        $field = $filter['field'];
                        $value = "'%" . $filter['value'] . "'";
                        break;
                    case 'eq':
                        $compare = "%20%3D%20";
                        $field = $filter['field'];
                        $value = "'" .$filter['value'] . "'";
                        break;
                    case 'gt':
                        $compare = "%20%3E%20";
                        $field = $filter['field'];
                        $value = $filter['value'];
                        break;
                    case 'lt':
                        $compare = "%20%3C%20";
                        $field = $filter['field'];
                        $value = $filter['value'];
                        break;
                    case 'gte':
                        $compare = "%20%3E%3D%20";
                        $field = $filter['field'];
                        $value = $filter['value'];
                        break;
                    case 'lte':
                        $compare = "%20%3C%3D%20";
                        $field = $filter['field'];
                        $value = $filter['value'];
                        break;
                    case 'neq':
                        $compare = "%20%3C%3E%20";
                        $field = $filter['field'];
                        $value = "'" . $filter['value'] . "'";
                        break;
                }
                if ( $count == 0 && $intcount == 0 ) {
                    $before = "";
                    $end = "%20".$filters['logic']."%20";
                } elseif ( $count > 0 && $intcount == 0 ) {
                    $before = "";
                    $end = "%20".$filters['logic']."%20";
                } else {
                    $before = "%20".$filters['logic']."%20";
                    $end = "";
                }
                $where .= ( $nobegin ? "" : $before ) ."(". $field . $compare . $value .")";
                $count ++;
                $intcount ++;
            } else {
                $where .= "(" .parseFilters( $filter , $count ).")";
            }
            $where = str_replace(" or  or " , " or " , $where );
            $where = str_replace(" and  and " , " and " , $where );
        }
    } else {
        $where = "";
    }
    return $where;
}
$where = "";
$take = '';
$skip = '';
if(isset($_GET["take"]))
{
    $take = $_GET["take"];
}
if(isset($_GET["skip"]))
{
    $skip = $_GET["skip"];
}
if ( isset( $_REQUEST['filter'] )) $where = parseFilters( $_REQUEST['filter'] );
if ( !isset( $_REQUEST['sort'][0]['field'] ) ) { $field= "personId"; } else { $field = $_REQUEST['sort'][0]['field']; }
if ( !isset( $_REQUEST['sort'][0]['dir']   ) ) { $order = "desc"; } else { $order = $_REQUEST['sort'][0]['dir']; }
require 'apiConfig.php';
$config = new apiConfig();
$result = $config->getTableData('contactPerson?order='.$field.'%20'.$order.'&include_count=true&limit='.$take.'&offset='.$skip.'&filter='.$where);
header('Content-Type: application/json');
echo $result;
exit();