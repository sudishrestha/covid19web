<?php

require 'apiConfig.php';
$config = new apiConfig();
if($_POST['method'] == 'post')
{

    $data_arr = array(
        'region' => $_POST['region'],
        'confirmed' => $_POST['confirm'],
        'death' => $_POST['death'],
        'recovered' => $_POST['recover'],
        'dateUpdated' => date("Y-m-d h:m:s"),
        'status' => true,
    );

    $feed_data = array("resource" => $data_arr);
    $p_data = json_encode($feed_data);
    // echo $p_data;
    
    $result = $config->postData('covidData', $p_data);
    echo $result;

}

else if ($_POST['method'] == 'delete')
{
    echo $feed_id;
    $statId= $_POST["statId"];
    $data_arr = array(
        'id' => $statId
    );

    $feed_data = array("resource" => $data_arr);
    $p_data = json_encode($feed_data);
    $result = $config->deleteData('covidData', $p_data);
    echo $result;

}