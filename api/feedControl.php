<?php

require 'apiConfig.php';
$config = new apiConfig();
if($_POST['method'] == 'post')
{

    $icon=$_POST["iconImage"];
    $newsImg=$_POST["newsImage"];
    $src=$_POST["source"];
    $title=$_POST["title"];
    $desc=$_POST["description"];
    $link = $_POST["link"];
    $hasIcon = true;
    $hasNewsImage = true;
    $hasLink = true;
    if($icon =="")
    {
        $hasIcon = false;
    }
    if($newsImg =="")
    {
        $hasNewsImage = false;
    }
    if($link == "")
    {
        $hasLink = false;
    }


    $data_arr = array(
        'source' => $src,
        'feedTitle' => $title,
        'feedDescription' => $desc,
        'iconImage' => $icon,
        'newsImage' => $newsImg,
        'fullLink' => $link,
        'hasIconImage' => $hasIcon,
        'hasNewsImage' => $hasNewsImage,
        'hasLink' => $hasLink,
        'status' => true,
    );

    $feed_data = array("resource" => $data_arr);
    $p_data = json_encode($feed_data);
    // echo $p_data;
    
    $result = $config->postData('newsFeed', $p_data);
    echo $result;

}

else if ($_POST['method'] == 'delete')
{
    echo $feed_id;
    $feed_id= $_POST["feed_id"];
    $data_arr = array(
        'feedId' => $feed_id
    );

    $feed_data = array("resource" => $data_arr);
    $p_data = json_encode($feed_data);
    $result = $config->deleteData('newsFeed', $p_data);
    echo $result;

}