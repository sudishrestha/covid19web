<?php

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

class apiConfig
{
	

// Live credential
	public $url = 'http://api.sudishrestha.com.np/';
    public $imageUrl = 'http://api.sudishrestha.com.np/';
    public $email = 'sudish@gmail.com';
    public $password = 'possible';
    public $api_key = '6498a8ad1beb9d84d63035c5d1120c007fad6de706734db9689f8996707e0f7d';
    public $my_domain = "http://10.6.1.97:88";
    private $url_ep = '';
    private $url_sn = '';
    private $url_fn = '';
    private $system_url = '';
    private $crd_ep = array('username' => '', 'password' => '');
    private $crd_sn = array('username' => '', 'password' => '');
    private $crd_fn = array('username' => '', 'password' => '');
    public $db_servername = "localhost";
    public $db_username = "root";
    public $db_password = "root";
    public $dbname = "cov19";

	public function getDomain ()
	{
		return $this->my_domain;
	}

	public function getImageUrl ()
	{
		return $this->imageUrl;
	}

	public function getSystemUrl()
	{
		return $this->system_url;
	}
	public function getCredit ($type)
	{
		$data = '';
		switch ($type) {
			case'EP':
				$data = $this->crd_ep;
				break;
			case'SN':
				$data = $this->crd_sn;
				break;
			case'FN':
				$data = $this->crd_fn;
				break;
		}
		return $data;
	}

	public function getUrl ($type = null)
	{
		$path = '';
		if ($type) {
			switch ($type) {
				case'EP':
					$path = rtrim($this->url_ep, '/');
					break;
				case'SN':
					$path = rtrim($this->url_sn, '/');
					break;
				case'FN':
					$path = rtrim($this->url_fn, '/');
					break;
				case'EP_REST':
					$path = rtrim($this->url_ep, '/') . '/rest/V1';
					break;
				case'SN_REST':
					$path = rtrim($this->url_sn, '/') . '/rest/V1';
					break;
				case'FN_REST':
					$path = rtrim($this->url_fn, '/') . '/rest/V1';
					break;
			}
		} else {
			$path = $this->url;
		}
		return $path;
	}

	public function getEmail ()
	{
		return $this->email;
	}

	public function getPassword ()
	{
		return $this->password;
	}

	public function getApiKey ()
	{
		return $this->api_key;
	}

	public function getToken ()
	{
		return $_SESSION["covtok"];
	}
	public function getLogin ($logUser,$logPass)
	{
		$data_array = array(
			"email" => $logUser,
			"password" => $logPass,
		);

		$url = $this->getUrl() . "/api/v2/user/session";
		$headers = array(
			'Authorization: ' . $this->getApiKey(),
		);
		$ch = curl_init($url);
		$postString = http_build_query($data_array, '', '&');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		// $result = json_decode($response);
		// $atoken = $result->session_token;
		// $_SESSION["covtok"] = $atoken;
		return $response;
	}


	public function getSession ()
	{
		$data_array = array(
			"email" => $this->getEmail(),
			"password" => $this->getPassword(),
		);

		$url = $this->getUrl() . "/api/v2/user/session";
		$headers = array(
			'Authorization: ' . $this->getApiKey(),
		);
		$ch = curl_init($url);
		$postString = http_build_query($data_array, '', '&');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		$result = json_decode($response);
		$atoken = $result->session_token;
		$_SESSION["covtok"] = $atoken;
		return $atoken;
	}

    public function getFromProcedure($procedure)
    {
        $url1 = $this->getUrl() . '/api/v2/cov19/_proc/' . $procedure;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $return_data = curl_exec($curl);
        if (!$return_data) {
            die("Connection Failure");
        }
        curl_close($curl);
        return $return_data;
    }
	public function loginHeader ()
	{
		$header_array = array(
			'X-DreamFactory-Session-Token:' . $this->getSession(),
			'X-DreamFactory-Api-Key:' . $this->getApiKey(),
			'Content-Type: application/json',
		);
		return $header_array;
	}

	public function getHeader ()
	{
		$header_array = array(
			'X-DreamFactory-Session-Token:' . $this->getToken(),
			'X-DreamFactory-Api-Key:' . $this->getApiKey(),
			'Content-Type: application/json',
		);
		return $header_array;
	}

	public function loginData ($table_name)
	{
		$url1 = $this->getUrl() . '/api/v2/cov19/_table/' . $table_name;
		//return $url1;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->loginHeader());
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	public function getTableData ($table_name)
	{

		$this->getSession();
		$url1 = $this->getUrl() . '/api/v2/cov19/_table/' . $table_name;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	
	public function getCovData ($table_name)
	{

		$this->getSession();
		$url1 = $this->getUrl() . '/api/v2/' . $table_name;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	public function postData ($table_name, $post_data)
	{
		$url1 = $this->getUrl() . '/api/v2/cov19/_table/' . $table_name;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	public function deleteData ($table_name, $delete_data)
	{
		$url1 = $this->getUrl() . '/api/v2/cov19/_table/' . $table_name;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $delete_data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	public function deleteDataTable ($table_name)
	{
		$url1 = $this->getUrl() . '/api/v2/cov19/_table/' . $table_name;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
//		curl_setopt($curl, CURLOPT_POSTFIELDS, $delete_data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	public function updateData ($table_name, $update_data)
	{
		$url1 = $this->getUrl() . '/api/v2/cov19/_table/' . $table_name;
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeader());
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $update_data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		$return_data = curl_exec($curl);
		if (!$return_data) {
			die("Connection Failure");
		}
		curl_close($curl);
		return $return_data;
	}

	public function sessionChecker ()
	{
		$result = $this->getTableData("role_table?filter=(id%3d1)");
		$output = json_decode($result, true);
		if (isset($output["error"]) && sizeof($output["error"]) > 0 && strpos($output["error"]["message"], "Token has expired") !== false) {
			return 0;
			exit;
		} else {
			$this->getSession ();
			return 1;
		}
	}



}
