<?php

session_start();
if($_GET["logout"] == "yes")
{
	session_destroy();
}
else
{
	if ($_SESSION["covlog"] == "poscov19") {
		?>
		<script>location.href = 'home.php';</script>
		<?php
	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/coronavirus.png">
    <link rel="icon" type="image/png" href="assets\img\coronavirus.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>COVID 19 Web Admin | <?php echo $title ; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
</head>

<body class="nav-md footer_fixed">

<div class="login_wrapper" align="center">
    <img src="assets\img\coronavirus.png" height="200px" width="200px"/>
</br>

	<form method="post" onsubmit="return login();" style="width:35%;margin-top:5%;">
		<div>
			<input type="text" class="form-control" placeholder="Username" required="" id="us11" name="us11"/>
			</br>
		</div>
		<div>
			<input type="password" class="form-control" placeholder="Password" required="" id="pw11" name="pw11"/>
			</br>
		</div>
		<div>
			<input type="submit" class="btn btn-primary" value="Login">
		</div>
		<div class="clearfix"></div>
		<br/>
		<div>
			<p>©2020 All Rights Reserved. Possbile Health</p>
		</div>
	</form>
</div>
</body>

<!--   Core JS Files   -->
<script src="assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="assets/js/plugins/bootstrap-switch.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!--  Chartist Plugin  -->
<script src="assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
<script src="assets\customJs\custom_index.js"></script>


</html>
