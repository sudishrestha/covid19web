<div class="sidebar" data-image="assets/img/sidebar-5.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="http://www.creative-tim.com" class="simple-text">
                        COVID 19
                    </a>
                </div>
                <ul class="nav">
                    <li class="<?php echo $dash?>">
                        <a class="nav-link" href="home.php">
                            <i class="fa fa-tachometer"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="<?php echo $hospital?>">
                        <a class="nav-link" href="hospital.php">
                        <i class="fa fa-hospital-o"></i>
                            <p>Hospitals</p>
                        </a>
                    </li>
                    <li class="<?php echo $feeds?>">
                        <a class="nav-link" href="newsfeed.php">
                        <i class="fa fa-rss-square"></i>
                            <p>News Feeds</p>
                        </a>
                    </li>
                    <li class="<?php echo $emergen?>">
                        <a class="nav-link" href="emergency.php">
                        <i class="fa fa-phone"></i>
                            <p>Emergency Nos</p>
                        </a>
                    </li>
                    <li class="<?php echo $stats?>">
                        <a class="nav-link" href="stats.php">
                            <i class="fa fa-line-chart"></i>
                            <p>Stats</p>
                        </a>
                    </li>
                    <li class="<?php echo $suspects?>">
                        <a class="nav-link" href="suspects.php">
                            <i class="fa fa-users"></i>
                            <p> Suspects</p>
                        </a>
                    </li>
                  
                </ul>
            </div>
        </div>

        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#pablo"> <?php echo $title ; ?> </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown">
                                    <span class="d-lg-none"><?php echo $title ; ?></span>
                                </a>
                            </li>
                           
                        </ul>
                        <ul class="navbar-nav ml-auto">
                         
                            <li class="nav-item">
                                <a class="nav-link" href="#pablo">
                                    <span class="no-icon"><a href="index.php?logout=yes">Log out</a></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>