<?php

session_start();
	if ($_SESSION["covlog"] != "poscov19") {
		?>
		<script>location.href = 'index.php';</script>
		<?php
	}
?>

<!-- 
=========================================================
 Light Bootstrap Dashboard - v2.0.1
=========================================================

 Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  -->
 <!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/coronavirus.png">
    <link rel="icon" type="image/png" href="assets\img\coronavirus.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>COVID 19 Web Admin | <?php echo $title ; ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <link rel="stylesheet" href="vendors/kendo/kendo.common-material.min.css" />
    <link rel="stylesheet" href="vendors/kendo/kendo.material.min.css" />
    <link rel="stylesheet" href="assets/css/custom_css.css" />

    <script src="vendors/jquery/jquery.min.js"></script>
    
    
    <script src="vendors/kendo/kendo.all.min.js"></script>

</head>

<body>